package model;

import com.google.gson.annotations.SerializedName;

public class mdlBranch {
    @SerializedName(value = "branch_code", alternate = {"branch_cd"})
    public String branch_code;
    @SerializedName(value = "branch_name")
    public String branch_name;
    @SerializedName(value = "branch_initial")
    public String branch_initial;
    @SerializedName(value = "branch_type")
    public String branch_type;
    @SerializedName(value = "branch_coordinator")
    public String branch_coordinator;
    @SerializedName(value = "location_type")
    public String location_type;
    @SerializedName(value = "region_code", alternate = {"region_cd"})
    public String region_code;
    @SerializedName(value = "address", alternate = {"branch_address"})
    public String address;
    @SerializedName(value = "city", alternate = {"branch_city"})
    public String city;
    @SerializedName(value = "longitude", alternate = {"branch_longitude"})
    public String longitude;
    @SerializedName(value = "latitude", alternate = {"branch_latitude"})
    public String latitude;
    @SerializedName(value = "flag_reservation", alternate = {"reservation"})
    public String flag_reservation;
    @SerializedName(value = "flag_weekend_banking_saturday", alternate = {"weekend_bank_sat"})
    public String flag_weekend_banking_saturday;
    @SerializedName(value = "flag_weekend_banking_sunday", alternate = {"weekend_bank_sun"})
    public String flag_weekend_banking_sunday;
    @SerializedName(value = "vendor_kiosk", alternate = {"kiosk_vendor"})
    public String vendor_kiosk;
    @SerializedName(value = "regular_kiosk", alternate = {"reguler_kiosk"})
    public String regular_kiosk;
    @SerializedName(value = "prioritas_kiosk")
    public String prioritas_kiosk;
    @SerializedName(value = "timezone")
    public String timezone;
    @SerializedName(value = "kcu_code", alternate = "kcu_cd")
    public String kcu_code;
    @SerializedName(value = "updated_date")
    public String updated_date;
}
