package model;

import java.util.ArrayList;
import java.util.List;

public class mdlQueryTransaction {

    public String sql;
    public List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();

    public String getSql() {
	return sql;
    }

    public void setSql(String sql) {
	this.sql = sql;
    }

    public List<mdlQueryExecute> getListParam() {
	return listParam;
    }

    public void setListParam(List<mdlQueryExecute> listParam) {
	this.listParam = listParam;
    }

}
