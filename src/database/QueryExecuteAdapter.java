package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import oracle.jdbc.OracleConnection;
import org.apache.logging.log4j.Logger;

import adapter.LogAdapter;

import org.apache.logging.log4j.LogManager;
import model.mdlQueryExecute;
import model.mdlQueryTransaction;

public class QueryExecuteAdapter {
    static String source = "App Server";
    final static Logger logger = LogManager.getLogger(QueryExecuteAdapter.class);
    static String apiName = "BCAGetDevice";
    static String apiMethod = "GET";

    public static CachedRowSet QueryExecute(String sql, String function) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	OracleConnection connection = RowSetAdapter.getConnectionWL();
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	CachedRowSet rs = null;
	RowSetFactory rowSetFactory = null;
	try {
	    pstm = connection.prepareStatement(sql);
	    jrs = pstm.executeQuery();
	    rowSetFactory = RowSetProvider.newFactory();
	    rs = rowSetFactory.createCachedRowSet();
	    rs.populate(jrs);
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "Function : " + functionName + ", sql : "
		    + sql, "", "", ex.toString()), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return rs;
    }

    public static CachedRowSet QueryExecute(String sql, List<mdlQueryExecute> queryParam, String function) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	OracleConnection connection = RowSetAdapter.getConnectionWL();
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	CachedRowSet rs = null;
	RowSetFactory rowSetFactory = null;
	try {
	    pstm = connection.prepareStatement(sql);
	    if (queryParam.size() >= 0) {
		for (int i = 1; i <= queryParam.size(); i++) {
		    System.out.println("Parameter " + i + " : " + queryParam.get(i - 1).paramValue + " || Type : "
			    + queryParam.get(i - 1).paramType.toLowerCase());

		    switch (queryParam.get(i - 1).paramType.toLowerCase()) {
		    case "string":
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    case "int":
		    case "integer":
			pstm.setInt(i, (int) queryParam.get(i - 1).paramValue);
			break;
		    case "decimal":
		    case "double":
			pstm.setDouble(i, (double) queryParam.get(i - 1).paramValue);
			break;
		    case "boolean":
			pstm.setBoolean(i, (boolean) queryParam.get(i - 1).paramValue);
			break;
		    case "null":
			pstm.setNull(i, java.sql.Types.NULL);
			break;
		    default:
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    }
		}
	    }
	    jrs = pstm.executeQuery();
	    rowSetFactory = RowSetProvider.newFactory();
	    rs = rowSetFactory.createCachedRowSet();
	    rs.populate(jrs);
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "Function : " + functionName + ", sql : "
		    + sql, "", "", ex.toString()), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return rs;
    }

    public static Boolean QueryManipulate(String sql, List<mdlQueryExecute> queryParam, String function) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	OracleConnection connection = RowSetAdapter.getConnectionWL();
	PreparedStatement pstm = null;
	Boolean success = false;
	try {
	    pstm = connection.prepareStatement(sql);
	    if (queryParam.size() >= 0) {
		for (int i = 1; i <= queryParam.size(); i++) {
		    switch (queryParam.get(i - 1).paramType.toLowerCase()) {
		    case "string":
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    case "int":
		    case "integer":
			pstm.setInt(i, (int) queryParam.get(i - 1).paramValue);
			break;
		    case "decimal":
		    case "double":
			pstm.setDouble(i, (double) queryParam.get(i - 1).paramValue);
			break;
		    case "boolean":
			pstm.setBoolean(i, (boolean) queryParam.get(i - 1).paramValue);
			break;
		    case "null":
			pstm.setNull(i, java.sql.Types.NULL);
			break;
		    default:
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    }
		}
	    }
	    pstm.execute();
	    success = true;
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "Function : " + functionName + ", sql : "
		    + sql, "", "", ex.toString()), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return success;
    }

    public static Boolean QueryManipulate(String sql, String function) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	OracleConnection connection = RowSetAdapter.getConnectionWL();
	PreparedStatement pstm = null;
	Boolean success = false;
	try {
	    pstm = connection.prepareStatement(sql);
	    pstm.execute();
	    success = true;
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "Function : " + functionName + ", sql : "
		    + sql, "", "", ex.toString()), ex);
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return success;
    }

    public static mdlQueryExecute QueryParam(String type, Object value) {
	mdlQueryExecute param = new mdlQueryExecute();
	param.paramType = type;
	param.paramValue = value;
	return param;
    }

    public static Boolean QueryTransaction(List<mdlQueryTransaction> listMdlQueryTrans, String function) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	OracleConnection connection = RowSetAdapter.getConnectionWL();
	PreparedStatement pstm = null;
	Boolean success = false;
	String sql;
	List<mdlQueryExecute> queryParam;

	try {
	    connection.setAutoCommit(false);

	    for (mdlQueryTransaction mdlQueryTrans : listMdlQueryTrans) {
		pstm = null;
		sql = mdlQueryTrans.sql;
		queryParam = new ArrayList<mdlQueryExecute>();
		queryParam.addAll(mdlQueryTrans.listParam);
		pstm = connection.prepareStatement(sql);

		for (int i = 1; i <= queryParam.size(); i++) {
		    switch (queryParam.get(i - 1).paramType.toLowerCase()) {
		    case "string":
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    case "int":
		    case "integer":
			pstm.setInt(i, (int) queryParam.get(i - 1).paramValue);
			break;
		    case "decimal":
		    case "double":
			pstm.setDouble(i, (double) queryParam.get(i - 1).paramValue);
			break;
		    case "boolean":
			pstm.setBoolean(i, (boolean) queryParam.get(i - 1).paramValue);
			break;
		    case "null":
			pstm.setNull(i, java.sql.Types.NULL);
			break;
		    default:
			pstm.setString(i, (String) queryParam.get(i - 1).paramValue);
			break;
		    }
		}
		pstm.executeUpdate();
	    }
	    connection.commit(); // commit transaction if all of the proccess is running well
	    success = true;
	} catch (Exception ex) {
	    if (connection != null) {
		try {
		    System.err.print("Transaction is being rolled back");
		    connection.rollback();
		} catch (SQLException excep) {
		}
	    }
	    success = false;
	} finally {
	    try {
		if (pstm != null)
		    pstm.close();
		connection.setAutoCommit(true);
		if (connection != null)
		    connection.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, apiName, apiMethod, "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return success;
    }

}
