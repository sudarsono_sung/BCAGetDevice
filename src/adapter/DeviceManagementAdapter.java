package adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.rowset.CachedRowSet;

import com.bca.libapitoken.repository.ApiTokenRepository;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.google.gson.Gson;
import database.QueryExecuteAdapter;
import model.mdlAPIResult;
import model.mdlBranch;
import model.mdlDeviceManagement;
import model.mdlLog;
import model.mdlQueryExecute;
import model.mdlServiceHour;

public class DeviceManagementAdapter {
    private static Logger logger = LogManager.getLogger(DeviceManagementAdapter.class);
    static Gson gson = new Gson();

    public static model.mdlDeviceManagement GetDeviceManagement(String SerialNumber) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        CachedRowSet jrs = null;
        model.mdlDeviceManagement DeviceManagement = new model.mdlDeviceManagement();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();

        try {
            // set sql string
            String sql = "SELECT DEVICE.SERIALNUMBER, DEVICE.WSID, DEVICE.BRANCHCODE, BRANCH.BRANCHNAME, DEVICE.BRANCHTYPEID, "
                    + "DEVICE.BRANCHINITIAL, BRANCHTYPE.BRANCHTYPENAME, DEVICE.IS_MASTER, DEVICE.ISMYBCA, DEVICE.IP_PRINTER, DEVICE.UPDATEBY, DEVICE.UPDATEDATE, "
                    + "SERVICEHOUR.SERVICE_HOUR1, SERVICEHOUR.SERVICE_HOUR2, SERVICEHOUR.SERVICE_HOUR3, "
                    + "SERVICEHOUR.MENU_ENABLED1, SERVICEHOUR.MENU_ENABLED2, SERVICEHOUR.MENU_ENABLED3 "
                    + "FROM MS_DEVICE DEVICE "
                    + "LEFT JOIN MS_BRANCH BRANCH ON DEVICE.BRANCHCODE = BRANCH.BRANCHCODE AND DEVICE.BRANCHTYPEID = BRANCH.BRANCHTYPEID AND DEVICE.BRANCHINITIAL = BRANCH.BRANCHINITIAL "
                    + "LEFT JOIN MS_BRANCHTYPE BRANCHTYPE ON DEVICE.BRANCHTYPEID = BRANCHTYPE.BRANCHTYPEID "
                    + "LEFT JOIN MS_SERVICE_HOUR SERVICEHOUR ON DEVICE.SERIALNUMBER = SERVICEHOUR.SERIAL_NUMBER "
                    + "WHERE DEVICE.SERIALNUMBER = ?";
            listParam.add(QueryExecuteAdapter.QueryParam("string", SerialNumber));
            jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

            while (jrs.next()) {
                DeviceManagement.SerialNumber = jrs.getString("SERIALNUMBER");
                DeviceManagement.WSID = jrs.getString("WSID");
                DeviceManagement.BranchCode = jrs.getString("BRANCHCODE");
                DeviceManagement.BranchInitial = jrs.getString("BRANCHINITIAL");
                DeviceManagement.BranchTypeID = jrs.getString("BRANCHTYPEID");
                DeviceManagement.BranchName = jrs.getString("BRANCHNAME") == null ? "" : jrs.getString("BRANCHNAME");
                DeviceManagement.BranchTypeName = jrs.getString("BRANCHTYPENAME") == null ? "" : jrs.getString("BRANCHTYPENAME");
                DeviceManagement.IsMaster = jrs.getString("IS_MASTER") == null ? "0" : jrs.getString("IS_MASTER");
                DeviceManagement.IsMyBca = jrs.getString("ISMYBCA") == null ? "0" : jrs.getString("ISMYBCA");
                DeviceManagement.IpPrinter = jrs.getString("IP_PRINTER") == null ? "127.0.0.1" : jrs.getString("IP_PRINTER");
                DeviceManagement.UpdateBy = jrs.getString("UPDATEBY");
                DeviceManagement.UpdateDate = jrs.getString("UPDATEDATE");
                mdlServiceHour mdlServiceHour = new mdlServiceHour();
                mdlServiceHour.ServiceHour1 = jrs.getString("SERVICE_HOUR1");
                mdlServiceHour.ServiceHour2 = jrs.getString("SERVICE_HOUR2");
                mdlServiceHour.ServiceHour3 = jrs.getString("SERVICE_HOUR3");
                mdlServiceHour.MenuEnabled1 = jrs.getString("MENU_ENABLED1");
                mdlServiceHour.MenuEnabled2 = jrs.getString("MENU_ENABLED2");
                mdlServiceHour.MenuEnabled3 = jrs.getString("MENU_ENABLED3");
                DeviceManagement.ServiceHour = mdlServiceHour;
            }
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetDevice", "GET", "function: " + functionName, "", "", ex.toString()), ex);
        }
        return DeviceManagement;
    }

    public static model.mdlDeviceManagement insertDeviceServiceHourData(model.mdlDeviceManagement deviceManagement) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
        try {
            String sql = "INSERT INTO MS_SERVICE_HOUR (SERIAL_NUMBER) VALUES (?)";
            listParam.add(QueryExecuteAdapter.QueryParam("string", deviceManagement.SerialNumber));
            boolean success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
            if (success) {
                deviceManagement.ServiceHour = GetDeviceServiceHourData(deviceManagement.SerialNumber);
            } else {
                mdlServiceHour mdlServiceHour = new mdlServiceHour();
                mdlServiceHour.ServiceHour1 = "17:59";
                mdlServiceHour.ServiceHour2 = "";
                mdlServiceHour.ServiceHour3 = "";
                mdlServiceHour.MenuEnabled1 = "CETAK BUKU TABUNGAN, MBCA";
                mdlServiceHour.MenuEnabled2 = "";
                mdlServiceHour.MenuEnabled3 = "";
                deviceManagement.ServiceHour = mdlServiceHour;
            }
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetDevice", "GET", "function: " + functionName, "", "", ex.toString()), ex);
        }
        return deviceManagement;
    }

    public static mdlServiceHour GetDeviceServiceHourData(String serialNumber) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
        CachedRowSet jrs = null;
        mdlServiceHour mdlServiceHour = new mdlServiceHour();
        try {
            String sql = "SELECT * FROM ms_service_hour WHERE serial_number = ? ";
            listParam.add(QueryExecuteAdapter.QueryParam("string", serialNumber));
            jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
            while (jrs.next()) {
                mdlServiceHour.ServiceHour1 = jrs.getString("SERVICE_HOUR1");
                mdlServiceHour.ServiceHour2 = jrs.getString("SERVICE_HOUR2");
                mdlServiceHour.ServiceHour3 = jrs.getString("SERVICE_HOUR3");
                mdlServiceHour.MenuEnabled1 = jrs.getString("MENU_ENABLED1");
                mdlServiceHour.MenuEnabled2 = jrs.getString("MENU_ENABLED2");
                mdlServiceHour.MenuEnabled3 = jrs.getString("MENU_ENABLED3");
            }
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetDevice", "GET", "function: " + functionName, "", "", ex.toString()), ex);
        }
        return mdlServiceHour;
    }

    public static boolean CheckBranchFromDatabase(mdlDeviceManagement device) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
        CachedRowSet jrs = null;
        boolean exists = false;
        try {
            // set sql string
            String sql = "SELECT 1 FROM ms_branch WHERE branchcode = ? AND branchtypeid = ? AND branchinitial = ? ";
            listParam.add(QueryExecuteAdapter.QueryParam("string", device.BranchCode));
            listParam.add(QueryExecuteAdapter.QueryParam("string", device.BranchTypeID));
            listParam.add(QueryExecuteAdapter.QueryParam("string", device.BranchInitial));
            jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
            while (jrs.next()) {
                exists = true;
            }
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetDevice", "GET", "function: " + functionName, "", "", ex.toString()), ex);
        }
        return exists;
    }

    public static String GetLastUpdatedDateBranch(mdlDeviceManagement device) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
        CachedRowSet jrs = null;
        String lastUpdatedDate = "";
        try {
            // set sql string
            String sql = "SELECT to_char(updateddate, 'YYYY-MM-DD') FROM ms_branch WHERE branchcode = ? AND branchtypeid = ? AND branchinitial = ? ";
            listParam.add(QueryExecuteAdapter.QueryParam("string", device.BranchCode));
            listParam.add(QueryExecuteAdapter.QueryParam("string", device.BranchTypeID));
            listParam.add(QueryExecuteAdapter.QueryParam("string", device.BranchInitial));
            jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
            while (jrs.next()) {
                lastUpdatedDate = jrs.getString(1) == null ? "" : jrs.getString(1);
            }
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetDevice", "GET", "function: " + functionName, "", "", ex.toString()), ex);
        }
        return lastUpdatedDate;
    }

    public static mdlAPIResult hitApiInquiryBranch(mdlDeviceManagement device) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        String apiFunction = "BranchInquiry";
        mdlAPIResult mdlBranchInquiryResult = new mdlAPIResult();
        mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = apiFunction;
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Middleware";
        mdlLog.SerialNumber = device.SerialNumber;
        mdlLog.WSID = device.WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";
        String jsonIn = "";
        String jsonOut = "";
        String urlApi = String.format("/midtier/branches/api/contains?branch-code=%s", device.BranchCode);
        String apiMethod = "GET";
        try {
            // Get the base naming context from web.xml
            Context context = (Context) new InitialContext().lookup("java:comp/env");
            // Get a single value from web.xml
            String urlGetToken = (String) context.lookup("param_ip_token");
            String ipAddressApiGateway = (String) context.lookup("param_ip_api_gw");
            String keyApi = (String) context.lookup("param_key_api");
            String clientIdApiGateway = EncryptAdapter.decrypt((String) context.lookup("param_clientid"), keyApi);
            String clientSecretApiGateway = EncryptAdapter.decrypt((String) context.lookup("param_clientsecret"), keyApi);
            String xBcaClientId = EncryptAdapter.decrypt((String) context.lookup("param_xbca_clientid"), keyApi);
            String userId = EncryptAdapter.decrypt((String) context.lookup("param_user_id"), keyApi);

            ApiTokenRepository apiTokenRepository = new ApiTokenRepository(urlGetToken, clientIdApiGateway, clientSecretApiGateway, ipAddressApiGateway);
            Map<String, String> headerList = new HashMap<>();
            headerList.put("client-id", xBcaClientId);
            headerList.put("user-id", userId);
            jsonOut = apiTokenRepository.callApiGateway(urlApi, apiMethod, headerList, "");

            if (jsonOut.equals("")) {
                mdlBranchInquiryResult = null;
                mdlLog.ErrorMessage = "API output is null";
                logger.error(LogAdapter.logToLog4j(false, startTime, 400, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
            } else {
                mdlBranchInquiryResult = gson.fromJson(jsonOut, mdlAPIResult.class);
                if (mdlBranchInquiryResult.ErrorSchema.ErrorCode == null) {
                    mdlLog.ErrorMessage = "API output is null";
                    logger.error(LogAdapter.logToLog4j(false, startTime, 500, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
                } else if (!mdlBranchInquiryResult.ErrorSchema.ErrorCode.equalsIgnoreCase("D000")) {
                    mdlLog.ErrorMessage = mdlBranchInquiryResult.ErrorSchema.ErrorMessage.English;
                    logger.error(LogAdapter.logToLog4j(false, startTime, 400, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
                } else {
                    mdlLog.LogStatus = "Success";
                    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
                }
            }
        } catch (Exception ex) {
            mdlBranchInquiryResult = null;
            logger.error(LogAdapter.logToLog4jException(startTime, 500, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut, ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
        }
        LogAdapter.InsertLog(mdlLog);
        return mdlBranchInquiryResult;
    }

    public static boolean UpdateBranchData(mdlBranch branch, String wsid) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
        boolean success = false;


        try {
            String sql = "UPDATE ms_branch SET branchname = ?, kanwil = ? , branchcoordinator = ?, updateddate = current_timestamp, "
                    + "updatedby = ?, location_type = ?, address = ? , city = ?, longitude = ?, latitude = ?, "
                    + "flag_reservation = ?, flag_weekend_banking_saturday = ?, flag_weekend_banking_sunday = ?, vendor_kiosk = ?, regular_kiosk = ?, "
                    + "prioritas_kiosk = ?, timezone = ?, kcu_code = ? WHERE branchcode = ? AND branchtypeid = ? "
                    + "AND branchinitial = ? ";
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.branch_name));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.region_code));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.branch_coordinator));

            listParam.add(QueryExecuteAdapter.QueryParam("string", wsid == null ? "" : wsid));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.location_type));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.address));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.city));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.longitude));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.latitude));

            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.flag_reservation));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.flag_weekend_banking_saturday));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.flag_weekend_banking_sunday));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.vendor_kiosk));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.regular_kiosk));

            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.prioritas_kiosk));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.timezone));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.kcu_code));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.branch_code));
            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.branch_type));

            listParam.add(QueryExecuteAdapter.QueryParam("string", branch.branch_initial));
            success = QueryExecuteAdapter.QueryManipulate(sql, listParam, functionName);
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAGetDevice", "GET", "Function : " + functionName + ", mdlBranch : "
                    + gson.toJson(branch), "", "", ex.toString()), ex);
        }
        return success;
    }
}